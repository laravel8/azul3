<?php

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();
//anotacion
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/user', 'UserController@index')->name('user.index');
Route::get('/user/{id}/edit','UserController@edit')->name('user.edit');
Route::get('/user/{id}/delete','UserController@destroy')->name('user.destroy');
Route::get('/user/create','UserController@create')->name('user.create');
Route::post('/user/create','UserController@store')->name('user.store');
Route::post('/user/update','UserController@update')->name('user.update');
//prueba de fuego
