@extends('layouts.app')

@section('content')
<div class="container">
    <h2>Lista de Usuarios <a href="{{ route('user.create') }}"><button type="button" class="btn btn-success float-right">New user</button></a></h2>
        <table class="table table-hover">
        <thead>
            <tr>
            <th scope="col">N°</th>
            <th scope="col">Nombre</th>
            <th scope="col">Email</th>
            <th scope="col">Role</th>
            <th scope="col">Action</th>
            </tr>
        </thead>
        <tbody>
            <?php
                $i=1;
            ?>
            @foreach($users as $user)
                @if ($user->role->name!='Master')
                <tr>
                <th scope="row"><?php
                    echo $i++;
                ?></th>
                <td>{{$user->name}}</td>
                <td>{{$user->email}}</td>
                <td>{{$user->role->name}}</td>
                <td>
                    <a href="{{ route('user.edit',['id'=>$user->id]) }}"><button type="button" class="btn btn-warning">Edit</button></a>
                    <a href="{{ route('user.destroy',['id'=>$user->id]) }}"><button type="button" class="btn btn-danger">Delete</button></a>
                </td>
                </tr>
                @endif
            @endforeach
        </tbody>
        </table>

</div>
@endsection
