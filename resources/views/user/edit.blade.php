@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
            <div class="col-sm-4">
            <form action="{{ route('user.update') }}" method="POST">
                @csrf
            <div class="form-group">
                <label for="name">Nombre</label>
                <input value="{{ $user->name }}" type="text" class="form-control" id="name" name="name" placeholder="Juan Perez">
            </div>
            <div class="form-group">
                <label for="email">Email address</label>
                <input value="{{ $user->email }}" type="text" class="form-control" id="email" name="email" placeholder="name@example.com">
            </div>
            <div class="form-group">
                <label for="role_id">Rol</label>
                <select class="form-control" id="role_id" name="role_id" >
                    @foreach ($roles as $role )
                    @if ($role->name!='Master')
                        @if ($user->role_id == $role->id )
                        <option selected value="{{$role->id}}">{{$role->name}}</option>
                        @else
                        <option value="{{$role->id}}">{{$role->name}}</option>
                        @endif
                    @endif
                    @endforeach
                </select>
            </div>
            <input type="hidden" name="id" value = "{{$user->id}}">
            <button type="submit" class="btn btn-primary">Submit</button>
            </form>
        </div>
    </div>
</div>
@endsection
