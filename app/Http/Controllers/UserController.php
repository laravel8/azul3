<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User as User;
use App\Model\Role;
class UserController extends Controller
{

    public function index()
    {
        //
        $users= User::all();


        return view('user.index',['users' => $users]);
    }


    public function create()
    {
        $roles=Role::all();
        return view('user.create',['roles' => $roles]);
    }


    public function store(Request $request)
    {
        //falta validacion de correo unico
        User::create($request->all());
        $users= User::all();
        return view('user.index',['users' => $users]);
    }


    public function show($id)
    {
        //
    }


    public function edit($id)
    {
        $roles=Role::all();
        $user = User::find($id);
        return view('user.edit',['user'=> $user],['roles' => $roles]);


    }



    public function update(Request $request)
    {
        //dd($request('id'));
        $user = User::find($request->id);
        $user->name = $request->name;
        $user->role_id = $request->role_id;
        $user->email = $request->email;
        $user->save(); //persist the data
        return redirect()->route('user.index');
    }



    public function destroy($id)
    {
        $user = User::find($id);

        $user->delete();
        return redirect()->route('user.index');
    }
}
